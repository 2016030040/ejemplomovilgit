package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private EditText txtNum1;
    private EditText txtNum2;
    private EditText txtResult;
    private Button btnSumar, btnRestar, btnDividir, btnMultiplicar, btnCerrar, btnLimpiar;
    private Operaciones op = new Operaciones();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }


    private void initComponents() {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResult = (EditText) findViewById(R.id.txtResult);

        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);
        btnDividir = (Button) findViewById(R.id.btnDivi);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
    }

    public void setEventos() {
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMultiplicar.setOnClickListener(this);
        this.btnDividir.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSuma:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResult.setText("Resultado: "+op.suma());

                break;
            case R.id.btnResta:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResult.setText("Resultado: "+op.resta());

                break;

            case R.id.btnMult:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResult.setText("Resultado: "+op.mult());

                break;
            case R.id.btnDivi:
                op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                txtResult.setText("Resultado: "+op.div());

                break;
            case R.id.btnLimpiar:
                txtNum1.setText("");
                txtNum2.setText("");
                txtResult.setText("");
                break;
            case R.id.btnCerrar:
                finish();
                break;
        }

    }
}
